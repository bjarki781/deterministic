// https://github.com/smileycoin/smileyCoin/blob/master/src/chainparams.cpp

var common = {
  name: 'Smileycoin',
  unit: 'SMLY'
}

var main = Object.assign({}, {
  hashGenesisBlock: '0x660f734cf6c6d16111bde201bbd2122873f2f2c078b969779b9d4c99732354fd',
  port: 11337,
  protocol: {
    magic: 0xf9beb4d9
  },
  seedsDns: [
    'smileyco.in',
    'dnsseed.smileyco.in'
  ],
  versions: {
    bip32: {
      private: 0x1e562d9a,
      public: 0x1e5631bc
    },
    bip44: 1,
    private: 0x99,
    public: 0x19,
    scripthash: 0x05
  }
}, common)

var test = Object.assign({}, {
  hashGenesisBlock: '0x54810bfb46c7b0d7bbe184faa10d2352810b29d1cdfa5169ce3aed387d80b921',
  versions: {
    bip44: 1,
    private: 0x70,
    public: 0x3a,
    scripthash: 0x0c
  }
}, common)

module.exports = {
  main,
  test
}

