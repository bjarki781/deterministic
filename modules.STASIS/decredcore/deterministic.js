// (C) 2018 Internet of Coins / Joachim de Koning
// Deterministic encryption wrapper for Decred

wrapperlib = require('./wrapperlib');

var wrapper = (
  function () {

    var functions = {
      // create deterministic public and private keys based on a seed
      // https://github.com/dashevo/decredcore-lib/blob/master/docs/examples.md
      keys : function(data) {
        var seed = new Buffer(data.seed);
        var hash = wrapperlib.decredcore.crypto.Hash.sha256(seed);
        var bn = wrapperlib.decredcore.crypto.BN.fromBuffer(hash);

        var privateKey = new wrapperlib.decredcore.PrivateKey(bn, data.mode);
        var wif = privateKey.toWIF();

        return { WIF: wif };
      },

      // generate a unique wallet address from a given public key
      address : function(data) {
        var privateKey = new wrapperlib.decredcore.PrivateKey(data.WIF, data.mode);
        var address = privateKey.toAddress();
        if (!wrapperlib.decredcore.Address.isValid(address, data.mode)) {
          throw new Error("Can't generate address from private key. "
                             + "Generated address " + address
                             + "is not valid for " + data.mode);
        }

        return address.toString();
      },

      // return public key
      publickey : function(data) {
        var privKey = wrapperlib.decredcore.PrivateKey(data.WIF, data.mode);
        return new wrapperlib.decredcore.PublicKey(privKey).toString('hex');
      },

      // return private key
      privatekey : function(data) {
        return data.WIF;
      },

      transaction : function(data) {
        var privKey       = wrapperlib.decredcore.PrivateKey(data.keys.WIF, data.mode);
        var recipientAddr = wrapperlib.decredcore.Address(data.target, data.mode);
        var changeAddr    = wrapperlib.decredcore.Address(data.source, data.mode);

        var tx = new wrapperlib.decredcore.Transaction()
          .from(data.unspent.unspents.map(function(utxo){
                  return { txId:        utxo.txid,
                           outputIndex: utxo.txn,
                           address:     utxo.address,
                           script:      utxo.script,
                           satoshis:    parseInt(utxo.amount)
                         };
                }))
          .to(recipientAddr, parseInt(data.amount))
          .fee(parseInt(data.fee))
          .change(changeAddr)
          .sign(privKey);
          
        return tx.serialize();
      }
    }

    return functions;
  }
)();

// export the functionality to a pre-prepared var
window.deterministic = wrapper;
