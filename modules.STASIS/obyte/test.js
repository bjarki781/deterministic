const obyte = require('obyte');

//
// seed words: https://runkit.com/tarmo888/generate-wallet-addresses-from-seed-words
// more info contained in: https://github.com/bonustrack/obyte.js/blob/master/src/client.js#L47
// pushing tx: await client.broadcast(unit);
// https://github.com/Papabyte/Byteduino/blob/master/src/payment.cpp

/*
 * 
 * What makes Obyte a challenge to implement is that the library Obyte.js
 * is mostly integrated with its websocket connectivity, which makes it
 * hard to separate the deterministic and unspent parts from the
 * transaction push/broadcast part.
 * 
 */

const client = new obyte.Client(null, {reconnect: false, testnet: false});
//const client = new obyte.Client();

/*
const r = client.compose.message('payment',
        {
            outputs: [
                { address: 'OILVGJSUVT2WX4JJCQF3CDNR76U3ST2D', amount: 1000 }
            ]
        },
        '92PCDLxvCuwHYaVewXebu6DCn2m2NeV5YmYzAKMuMC4XNaH9CAs');
console.log(r);
*/
