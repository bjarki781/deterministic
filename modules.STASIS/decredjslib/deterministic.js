// (C) 2018 Internet of Coins / Metasync / Joachim de Koning
// Deterministic encryption wrapper for Decred
//
var wrapperlib = require('./wrapperlib');

var wrapper = (
  function() {

    var base58 = require('bs58');
    var ecurve = require('ecurve');
    var BigInteger = require('bigi');

    function setNetwork(data) {
        return 'decred';
    }

    var functions = {
      // create deterministic public and private keys based on a seed
      keys : function(data) {
        // return deterministic transaction data
        var network = setNetwork(data);
        var hash = wrapperlib.crypto.sha256(data.seed);
        var privk = BigInteger.fromBuffer(hash);

        var keyPair = new wrapperlib.ECPair(privk);

        var wif = keyPair.toWIF();
        return { WIF:wif };                 // returns object { WIF:'Kxr9tQED9H44gCmp6HAdmemAzU3n84H3dGkuWTKvE23JgHMW8gct' }
      },

      // generate a unique wallet address from a given public key
      address : function(data) {
        // return deterministic transaction data
        var network = setNetwork(data);
        var keyPair = wrapperlib.ECPair.fromWIF(data.WIF,wrapperlib.networks[network]);
        return keyPair.getAddress();
      },

      // return public key
      publickey : function(data) {
        var network = setNetwork(data);
        var keyPair = wrapperlib.ECPair.fromWIF(data.WIF,wrapperlib.networks[network]);
        return keyPair.getPublicKeyBuffer().toString('hex');
      },

      // return private key
      privatekey : function(data) {
        return data.WIF;
      },

      transaction : function(data) {
        // return deterministic transaction data
        var network = setNetwork(data);
        var keyPair = wrapperlib.ECPair.fromWIF(data.keys.WIF,wrapperlib.networks[network]);
        var tx = new wrapperlib.TransactionBuilder(wrapperlib.networks[network]);

        // add inputs
        for(var i in data.unspent.unspents) {
          tx.addInput(data.unspent.unspents[i].txid,parseInt(data.unspent.unspents[i].txn));
        }

        // add spend amount output
        tx.addOutput(data.target,parseInt(data.amount));

        // send back change
        var outchange=parseInt(data.unspent.change);   // fee is already being deducted when calculating unspents
        if(outchange>0) { tx.addOutput(data.source,outchange); }

        // sign inputs
        for(var i in data.unspent.unspents) {
          tx.sign(parseInt(i),keyPair);
        }

        return tx.build().toHex();

      }
    }

    return functions;
  }
)();

// export the functionality to a pre-prepared var
window.deterministic = wrapper;
