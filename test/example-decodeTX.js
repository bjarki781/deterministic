const bitcoin = require('./modules/bitcoinjslib/bitcoinjs-lib');

var rawhex = '010000000001024313edeb8a501f544e33aebfdec8840f181e3e532dc598cbd4ab28a8f85b0b9701000000171600145081dc4af9d81479c4335f36a08f51cb2f54269affffff002331ae6ee632c8e0672bcaef3dde684c5695f02eb6f7fbacf6e2da0cd7dede850000000017160014be18f8a843924bfe2b8ee7b8e4a1b9fa4e890352ffffff000240420f000000000017a914bcde907c0b3eff718ada80faf87f0394090c220c87f4ef2e000000000017a914ff12fef2f4ee9aa0ca475972ebc2a77b95bf08f98702473044022033fe8feea19fd2ce956062539464568102c34c110fb20f13c6b466d5e23996a502200c8aab8db08e23c535c323c77066fc5b9e8e3a7a6e2a305846648108201edb78012102d715f8910b48b8fcee84bf2ac7cb7837e1b5f6e0895a9e59cf8f433b9b0e03210247304402202eccdd70d58472993fcdb6ce9b6b09f0e49eabd8d14d663a78f658ed811e48b902207fd9d68508049bc13de2d3a88db7fce5ea91cf73d9f242b6638849f43cf8e057012102bf7d5323ada5c4836c87e5d15b635b3d43c42759d6788ddcd45ee27b5d3ccf6200000000';

const tx = bitcoin.Transaction.fromHex(rawhex);

console.log(tx);

var rebuild = {};

/*rebuild.in1hash = arrayBufferToString(tx.ins[0].hash).toString();
rebuild.in1script = arrayBufferToString(tx.ins[0].script).toString();
rebuild.in2hash = arrayBufferToString(tx.ins[1].hash).toString();
rebuild.in2script = arrayBufferToString(tx.ins[1].script).toString();
*/
rebuild.in1hash = Buffer.from(tx.ins[0].hash).toString('hex');
rebuild.outscript1 = tx.outs[0];
rebuild.outscript2 = tx.outs[1];

console.log(rebuild);

function arrayBufferToString(buffer){

    var bufView = new Uint16Array(buffer);
    var length = bufView.length;
    var result = '';
    var addition = Math.pow(2,16)-1;

    for(var i = 0;i<length;i+=addition){

        if(i + addition > length){
            addition = length - i;
        }
        result += String.fromCharCode.apply(null, bufView.subarray(i,i+addition));
    }

    return result;

}
